package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("Null elements found");
        }

        // calculate height of pyramid
        int pyramidHeight = 0;
        int sum = 1;
        for (int i = 2; i < inputNumbers.size(); i++) {
            sum += i;
            if (inputNumbers.size() == sum) {
                pyramidHeight = i;
                break;
            }
        }
        int[][] pyramid;

        int pyramidWidth = pyramidHeight * 2 - 1;
        try {
            // init pyramid
            pyramid = new int[pyramidHeight][pyramidWidth];
        } catch (NegativeArraySizeException errSize) {
            throw new CannotBuildPyramidException("Incorrect parameters (width/height): " +
                    pyramidWidth + "/" + pyramidHeight + "" + " found");
        }

        Collections.sort(inputNumbers);
        ArrayList<Integer> tempInputList = new ArrayList<>(inputNumbers);

        // fill pyramid
        int elementsAmount = 1;
        for (int i = 0; i < pyramidHeight; i++) {
            for (int j = 0; j < elementsAmount; j++) {
                int indexOfNextElement;
                int firstElementInLineIndex = pyramidHeight - elementsAmount;
                indexOfNextElement = firstElementInLineIndex + j * 2;
                pyramid[i][indexOfNextElement] = tempInputList.get(j);
            }

            elementsAmount++;
            for (int k = 0; k <= i; k++) {
                tempInputList.remove(0);
            }
        }

        return pyramid;
    }
}